###--- auto_create ---###

This bash script is used to build both the terraform environment infrastructure and the Jenkins server using Ansible. It's primary processes are as follows:
  1. Enter terraform directory and run "apply" command to build the infrastructure.
  2. Export amazon web service (aws) credentials as "global variables" so they can be referenced later in Terraform/Ansible scripts.
  3. Activate aws environment to provide commands which allow terraform state files to be copied up to an S3 bucket.
  4. Copy the terraform state files from the s3 bucket into a directory (S3_vars) within the "JenkinsServer" directory.
  5. Parse the terraform state files to obtain the public subnet IDs which are then exported as global variables to be used during the creation of the Jenkins server.
  6. Execute the ansible playbook to create the Jenkins server with its pre-loaded jobs.
  7. Run "jobCurl" to start the jobs on the Jenkins server using a webhook.

###--- auto_destroy ---###

This bash script essentially just destroys everything that "auto_create" builds.


###--- Variables that users should change ---###

The code is currently configured to run on the creators local machine, thus, the following variables should be changed by any user intending to run the program:

---AWS Credentials---
AWS_DEFAULT_REGION
AWS_DEFAULT_PROFILE
AWS_ACCESS_KEY
AWS_SECRET_KEY
AWS_SESSION_TOKEN
AWS_SSH_KEY

---Local Directory Locations---
The program routinely cd's into relevant directories (terraform_Infrastructure,jenkinsServer,S3_vars), thus, these filepaths will need to be configured to their location on the user's machine.
SSH_ANSIBLE_PRIVATE (location of private SSH key)
PYTHON_INTERPRETER (location of python interpreter)
