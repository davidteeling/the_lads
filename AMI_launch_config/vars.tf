variable "aws_profile" {
  type    = string
  default = "academy2"
}

variable "aws_region" {
  type    = string
  default = "us-east-1"
}

variable "ec2_type" {
  type    = string
  default = "t3.large"
}
