data "aws_security_group" "lads" {
  filter {
    name   = "tag:Name"
    values = ["The Lads SecGrp"]
  }
}

data "aws_security_group" "ladsDB" {
  filter {
    name   = "tag:Name"
    values = ["The LadsDB SecGrp"]
  }
}

data "aws_security_group" "ladsPC" {
  filter {
    name   = "tag:Name"
    values = ["The LadsPC SecGrp"]
  }
}
