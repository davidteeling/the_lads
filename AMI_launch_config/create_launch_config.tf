resource "aws_launch_configuration" "asg_first_launch_conf" {
  image_id        = data.aws_ami.find.id
  instance_type   = var.ec2_type
  security_groups = [data.aws_security_group.lads.id, data.aws_security_group.ladsDB.id, data.aws_security_group.ladsPC.id]
  lifecycle {
    create_before_destroy = true
  }
}
